from nltk import pos_tag
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
from nltk.stem import WordNetLemmatizer
from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction import stop_words
import stopwordsiso
import spacy
import pandas as pd
import string
import re
import math
import numpy as np

newsgroups = fetch_20newsgroups(subset="test", remove=("headers", "footers", "quotes"))
nlp = spacy.load("en_core_web_sm")
stemmer = PorterStemmer()
lemmatizer = WordNetLemmatizer()
NUM_DOCS = 50

custom_stopwords = [
    "neil",
    "gandler",
    "info",
    "bonneville",
    "nntp",
    "bonnevilles",
    "le",
    "lse",
    "sse",
    "ssei",
    "eep",
    "dpg",
    "linux",
    "enposxtigu",
    "ricevos",
    "alion",
    "rick",
    "miller",
    "muskego",
    "wis",
    "usa",
    "rusnews",
    "netters",
    "fatwah",
    "pious",
    "ern",
    "imo",
    "ins",
    "kartik",
    "pc",
    "tmc",
    "sgi",
    "nth",
    "hst",
    "urso"
]


def lemmatization(word_list):
    words = []
    for word, tag in pos_tag(word_list):
        wntag = tag[0].lower()
        wntag = wntag if wntag in ['a', 's', 'r', 'n', 'v'] else None
        if not wntag:
            words.append(word)
        else:
            words.append(lemmatizer.lemmatize(word, pos=wntag))

    return words


def get_stopwords():
    spacy_stopwords = nlp.Defaults.stop_words
    nltk_stopwords = set(stopwords.words("english"))
    sklearn_stopwords = stop_words.ENGLISH_STOP_WORDS
    stopwords_iso = stopwordsiso.stopwords("en")

    all_stopwords = set()
    all_stopwords |= spacy_stopwords
    all_stopwords |= nltk_stopwords
    all_stopwords |= sklearn_stopwords
    all_stopwords |= stopwords_iso
    all_stopwords |= set(custom_stopwords)

    return list(all_stopwords)


def remove_punctuation(wordlist):
    words = [word for word in wordlist if word not in string.punctuation]
    return words


def remove_regexp(wordlist):
    regex = re.compile(r"([.:=()@\-\\\/,\[\]\{\}\"\'\*\>\<\|\~\^]|(\d|\s))+")
    words = [word for word in wordlist if regex.search(word) is None]

    return words


def remove_stopwords(wordlist):
    words = [word for word in wordlist if word not in stop_words.words("english")]
    return words


def wordListToFreqList(wordlist):
    wordfreq = [wordlist.count(p) for p in wordlist]
    return str(list(zip(wordlist, wordfreq)))


my_dict = {
    "Document": newsgroups.filenames[:NUM_DOCS],
    "Category_num": newsgroups.target[:NUM_DOCS],
    "Category": [newsgroups.target_names[i] for i in newsgroups.target[:NUM_DOCS]],
    "Text": newsgroups.data[:NUM_DOCS],
    "Tokens": [],
    "Lemmas": [],
    "Stems": [],
    "Clean_lemmas": []
}



for i in range(0, NUM_DOCS):
    text = newsgroups.data[i]
    tokens = [token.text for token in nlp(text)]
    my_dict["Tokens"].append(tokens)
    tokens = remove_punctuation(tokens)
    tokens = remove_regexp(tokens)
    tokens = [token.lower() for token in tokens]
    lemmas = lemmatization(tokens)
    my_dict["Lemmas"].append(lemmas)
    lemmas_sin_stopwords = [lemma for lemma in lemmas if lemma not in get_stopwords()]
    my_dict["Clean_lemmas"].append(lemmas_sin_stopwords)
    stems = [stemmer.stem(token) for token in tokens if token not in get_stopwords()]
    my_dict["Stems"].append(stems)

my_corpus = pd.DataFrame(data=my_dict)

print(my_corpus)

# ------------------------------------------------------------

for terms in my_corpus["Clean_lemmas"]:
    vocabulary_list += terms

print(len(vocabulary_list))


weight_terms = pd.DataFrame(data={"Terms": vocabulary_list}).drop_duplicates()


with open("/home/begemot/Escritorio/prueba_di_5/Vocabulario.txt", "w") as archivo:
    for term in weight_terms["Terms"]:
        archivo.write(term + " ")
    archivo.close()


for idx, doc in enumerate(my_corpus["Clean_lemmas"]):
    weight_terms["Doc_" + str(idx)] = [doc.count(x) for x in weight_terms["Terms"]]

print(weight_terms)


with open("/home/begemot/Escritorio/prueba_di_5/Representaciones_TF.txt", "w") as archivo:
    for i in range(0, NUM_DOCS):
        for peso in weight_terms["Doc_" + str(i)]:
            archivo.write(str(peso + 1) + " ")
        archivo.write("\n")
    archivo.close()


freq_terms = pd.DataFrame(data={"Terms": weight_terms['Terms']})

for i in range(0, NUM_DOCS):
    freq_terms["Doc_" + str(i)] = [peso/float(sum(weight_terms["Doc_" + str(i)])) for peso in weight_terms["Doc_" + str(i)]]

print(freq_terms)


with open("/home/begemot/Escritorio/prueba_di_5/Fichero_invertido_WTF.txt", "w") as archivo:
    for idx, row in freq_terms.iterrows():
        [archivo.write(str(value) + "\t->\t") if i == 0 else archivo.write("doc" + str(i) + ":" + str(value) + "\t") for i, value in enumerate(list(row))]
        archivo.write("\n")
    archivo.close()


idf_terms = pd.DataFrame(data={"Terms": weight_terms['Terms']})


counts_apariciones = []
for idx, row in weight_terms.iterrows():
    counts_apariciones.append(len([cell for idx, cell in enumerate(list(row)) if idx != 0 and cell != 0]))

idf_terms["IDF"] = [math.log(NUM_DOCS/float(count)) for count in counts_apariciones]

print(idf_terms)


with open("/home/begemot/Escritorio/prueba_di_5/Funcion_global_IDF.txt", "w") as archivo:
    for idx, row in idf_terms.iterrows():
        [archivo.write(str(value) + "\t->\t") if i == 0 else archivo.write(str(value)) for i, value in enumerate(list(row))]
        archivo.write("\n")
    archivo.close()


tfidf_terms = pd.DataFrame(data={"Terms": weight_terms['Terms']})

for i in range(0, NUM_DOCS):
    tfidf_terms["Doc_" + str(i + 1)] = np.multiply(freq_terms["Doc_" + str(i)], idf_terms["IDF"])

print(tfidf_terms)


with open("/home/begemot/Escritorio/prueba_di_5/Funcion_global_TF-IDF.txt", "w") as archivo:
    for idx, row in tfidf_terms.iterrows():
        [archivo.write(str(value) + "\t->\t") if i == 0 else archivo.write("doc" + str(i) + ":" + str(value) + "\t") for i, value in enumerate(list(row))]
        archivo.write("\n")
    archivo.close()
